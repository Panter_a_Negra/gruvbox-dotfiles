#!/bin/zsh

var=$(date +%m-%d-%Y)

pwd | while read route; do
    mkdir $route/$var
    cp -rf /home/pantera/.config/nvim/Templates/base1/* $route/$var
    cd $var && nvim EnzoCalvi.tex
done

