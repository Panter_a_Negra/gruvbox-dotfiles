# Gentoo Gruvbox Dotfiles
## Dependencies
1. [Nerd Fonts](https://www.nerdfonts.com/)
1. [Papirus](https://www.gnome-look.org/p/1166289/)
1. [Bibata Icon Pack](https://www.gnome-look.org/p/1197198/)
1. [Noto Sans KR](https://fonts.google.com/specimen/Noto+Sans+KR?query=noto+sans#glyphs)

## Screenshots
![Screen-01](/Screenshots/Screen-01.jpg)
![Screen-02](/Screenshots/Screen-02.jpg)
![Screen-03](/Screenshots/Screen-03.jpg)
